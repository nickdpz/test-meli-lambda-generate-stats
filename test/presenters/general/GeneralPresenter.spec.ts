import 'reflect-metadata';
import { GeneralPresenter } from '../../../src/presenters/general/GeneralPresenter';

describe('SFSavingAccountPresenter Test Suite', () => {
    it('generateAcceptedResponse should return an object with status code 200', async () => {
        const presenter = new GeneralPresenter();
        expect(presenter.generateOKResponse()).toEqual('OK');
    });

    it('generateAcceptedResponse should return an object with status code 500', async () => {
        const presenter = new GeneralPresenter();
        expect(presenter.generateInternalErrorResponse()).toEqual('INTERNAL ERROR');
    });
});
