/* eslint-disable @typescript-eslint/no-explicit-any */
import * as AWSMock from 'aws-sdk-mock';
import * as AWS from 'aws-sdk';
import { DynamoDatabaseAdapter } from '../../../src/adapters/database/dynamo/DynamoDatabaseAdapter';
import { DatabaseAdapter } from '../../../src/adapters/database/DatabaseAdapter';
import { WinstonLogger } from '../../../src/utils/logger/winston/WinstonLogger';

describe('DynamoDatabaseAdapter Test Suite', () => {
    it('adapater incrementMutant successfully', async () => {
        AWSMock.setSDKInstance(AWS);
        AWSMock.mock(
            'DynamoDB.DocumentClient',
            'update',
            (parameters: any, callback: (str: string, result: any) => void) => {
                console.log('Intro mock dynamo success');
                callback(null, {});
            }
        );
        const adapter: DatabaseAdapter = new DynamoDatabaseAdapter(new WinstonLogger());
        await expectAsync(adapter.incrementMutant()).toBeResolved();
        AWSMock.restore('DynamoDB.DocumentClient');
    });
    it('adapater incrementHuman successfully', async () => {
        AWSMock.setSDKInstance(AWS);
        AWSMock.mock(
            'DynamoDB.DocumentClient',
            'update',
            (parameters: any, callback: (str: string, result: any) => void) => {
                console.log('Intro mock dynamo success');
                callback(null, {});
            }
        );
        const adapter: DatabaseAdapter = new DynamoDatabaseAdapter(new WinstonLogger());
        await expectAsync(adapter.incrementHuman()).toBeResolved();
        AWSMock.restore('DynamoDB.DocumentClient');
    });
    it('adapater throw error', async () => {
        AWSMock.setSDKInstance(AWS);
        AWSMock.mock(
            'DynamoDB.DocumentClient',
            'update',
            (parameters: any, callback: (str: string) => void) => {
                console.log('Intro mock dynamo error');
                callback('Error');
            }
        );
        const adapter: DatabaseAdapter = new DynamoDatabaseAdapter(new WinstonLogger());
        await expectAsync(adapter.incrementHuman()).toBeRejected();
        AWSMock.restore('DynamoDB.DocumentClient');
    });
});
