import 'reflect-metadata';
import { DatabaseAdapter } from '../DatabaseAdapter';
import { CONSTANTS, TYPES } from '../../../utils/Constants';
import { DocumentClient } from 'aws-sdk/clients/dynamodb';
import { inject, injectable } from 'inversify';
import { Logger } from '../../../utils/logger/Logger';

@injectable()
export class DynamoDatabaseAdapter implements DatabaseAdapter {
    private documentClient = new DocumentClient();

    constructor(@inject(TYPES.Logger) private LOGGER: Logger) {}

    async incrementHuman(): Promise<void> {
        return await this.incrementCount(CONSTANTS.COUNT_HUMAN_DNA);
    }

    async incrementMutant(): Promise<void> {
        return await this.incrementCount(CONSTANTS.COUNT_MUTANT_DNA);
    }

    public async incrementCount(type: string): Promise<void> {
        const parameters = {
            TableName: CONSTANTS.TABLE_NAME,
            Key: {
                DATA_TYPE: CONSTANTS.DATA_TYPE,
                DATE: CONSTANTS.DATE,
            },
            UpdateExpression: 'ADD #C :increment',
            ExpressionAttributeNames: {
                '#C': type,
            },
            ExpressionAttributeValues: {
                ':increment': 1,
            },
        };
        this.LOGGER.info('Save in dynamo', parameters);
        await this.documentClient.update(parameters).promise();
    }
}
