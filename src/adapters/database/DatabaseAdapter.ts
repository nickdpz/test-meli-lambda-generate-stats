export interface DatabaseAdapter {
    incrementHuman(): Promise<void>;
    incrementMutant(): Promise<void>;
}
