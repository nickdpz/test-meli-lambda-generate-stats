/* eslint-disable @typescript-eslint/no-explicit-any */
import { MainPresenter } from '../MainPresenter';
import { injectable } from 'inversify';

@injectable()
export class GeneralPresenter implements MainPresenter {
    generateInternalErrorResponse(): any {
        return 'INTERNAL ERROR';
    }

    generateOKResponse(): any {
        return 'OK';
    }
}
