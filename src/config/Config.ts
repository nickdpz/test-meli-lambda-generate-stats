import { Container } from 'inversify';
import { TYPES } from '../utils/Constants';
import { MainPresenter } from '../presenters/MainPresenter';
import { GeneralPresenter } from '../presenters/general/GeneralPresenter';
import { MainService } from '../services/MainService';
import { MainServiceImpl } from '../services/MainServiceImpl';
import { DatabaseAdapter } from '../adapters/database/DatabaseAdapter';
import { DynamoDatabaseAdapter } from '../adapters/database/dynamo/DynamoDatabaseAdapter';
import { MainController } from '../controllers/MainController';
import { DynamoDBController } from '../controllers/dynamodb/DynamoDBController';
import { WinstonLogger } from '../utils/logger/winston/WinstonLogger';
import { Logger } from '../utils/logger/Logger';

const AppContainer: Container = new Container();

AppContainer.bind<MainPresenter>(TYPES.MainPresenter).to(GeneralPresenter);
AppContainer.bind<MainService>(TYPES.MainService).to(MainServiceImpl);
AppContainer.bind<MainController>(TYPES.MainController).to(DynamoDBController);
AppContainer.bind<DatabaseAdapter>(TYPES.DatabaseAdapter).to(DynamoDatabaseAdapter);
AppContainer.bind<Logger>(TYPES.Logger).to(WinstonLogger);

export { AppContainer };
