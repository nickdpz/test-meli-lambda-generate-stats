# Test-Meli-Lambda-Generate-Stats

Lambda para general estadistica en reto mutantes de mercado libre 

## Manage repository 🤳

- Install dependencies
```
$ npm i
```
- Check code by linter
```
$ npm run lint
```
- Check and fix code by linter
```
$ npm run lint:fix
```
- Check style code
```
$ npm run prettier
```
- Check and fix style code
```
$ npm run prettier:fix
```
- Get coverage test terminal
```
$ npm run coverage
```

## Manage production environment 🎮

- Update Code Lambda
 
```sh
$ aws lambda update-function-code \
 --function-name meli-pro-is-mutant \
 --zip-file fileb://dist/main.zip \
 --publish \
 --output json
```

- Update lambda options
```sh
aws lambda update-function-configuration \
 --function-name meli-pro-is-mutant \
 --environment "Variables={NODE_ENV=pro}" 
```


